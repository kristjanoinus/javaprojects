package ee.kristjanoinus;

public class Printer {

    private int tonerLevel;
    private int pagesPrinted;
    private boolean duplex;

    public Printer(int tonerLevel, boolean duplex) {
        if (tonerLevel > -1 && tonerLevel <= 100) {
            this.tonerLevel = tonerLevel;
        } else {
            this.tonerLevel = -1;
        }
        this.duplex = duplex;
        this.pagesPrinted = 0;
    }

    public int getTonerLevel() {
        return tonerLevel;
    }

    public int getPagesPrinted() {
        return pagesPrinted;
    }

    public void setPagesPrinted(int pagesPrinted) {
        this.pagesPrinted = pagesPrinted;
    }

    public boolean isDuplex() {
        return duplex;
    }

    public void setDuplex(boolean duplex) {
        this.duplex = duplex;
    }

    public int fillToner(int tonerAmount) {
        if (tonerAmount > 0 && tonerAmount <= 100) {
            if (this.tonerLevel + tonerAmount > 100) {
                return -1;
            }
            this.tonerLevel += tonerAmount;
            return this.tonerLevel;
        } else {
            return -1;
        }
    }

    public void printPages (int pages, boolean bothSides) {
        if (bothSides && !duplex) {
            System.out.println("This printer does not allow duplex mode. Please disable both sides printing from printing options.");
        } else {
            for (int i = 1; i <= pages; i++) {
                if (this.tonerLevel > 0) {
                    if (bothSides) {
                        if (i % 2 == 0) {
                            System.out.println("Printing page " + (i / 2) + " backside");
                        } else {
                            this.pagesPrinted++;
                            System.out.println("Printing page " + (i / 2 + 1));
                        }
                        this.tonerLevel--;

                    } else {
                        this.pagesPrinted++;
                        this.tonerLevel--;
                        System.out.println("Printing page " + i);
                    }
                } else {
                    System.out.println("Cannot print. Toner out of ink.");
                }
            }
            System.out.println("Print job complete. Remaining tonerLevel = " + getTonerLevel() + ", Printer all time paperCount = " + getPagesPrinted());
        }
    }
}
