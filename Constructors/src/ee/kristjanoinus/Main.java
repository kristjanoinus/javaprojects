package ee.kristjanoinus;

public class Main {

    public static void main(String[] args) {

        BankAccount account = new BankAccount(12345, 10.00, "Bob K", "email@email.com", "+372679900");
        account.setBalance(1000);

        System.out.println(account.getBalance());

        account.withdraw(500);

        account.deposit(400);

        account.withdraw(10000);

        VipCustomer person1 = new VipCustomer();
        System.out.println(person1.getName());

        VipCustomer person2 = new VipCustomer("Bob", 2500.00);
        System.out.println(person2.getName());

        VipCustomer person3 = new VipCustomer("Tim", 2500.00, "h@mail.ee");
        System.out.println(person3.getName());
        System.out.println(person3.getEmail());


    }
}
