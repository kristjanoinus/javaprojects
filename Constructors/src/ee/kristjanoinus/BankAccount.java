package ee.kristjanoinus;

public class BankAccount {

    private long accountNumber;
    private double balance;
    private String customerName;
    private String email;
    private String phoneNumber;

    public BankAccount() {
        this(12444, 2.50, "Default name", "Default mail"
                , "Default phone"); // if this used as constructor, it needs to be 1st line in method.
        System.out.println("Empty constructor called");
    }

    public BankAccount(long accountNumber, double balance, String customerName, String email, String phoneNumber) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public BankAccount(String customerName, String email, String phoneNumber) {
        this(123444678, 150.0, customerName, email, phoneNumber);
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void deposit(double input) {
        this.balance += input;
        System.out.println(input + " was deposited on the account. Total balance is now " + this.balance);
    }

    public void withdraw(double withdrawal) {
        if (withdrawal > balance) {
            this.balance = this.balance + 0;
            System.out.println("The account balance is insufficient for withdrawal of " + withdrawal +
                    ". Please choose an amount of less than " + this.balance);
        } else {
            this.balance -= withdrawal;
            System.out.println(withdrawal + " was withdrawn from the account. The account balance is now "
                    + this.balance);
        }
    }
}
