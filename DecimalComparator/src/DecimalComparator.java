public class DecimalComparator {

    public static boolean areEqualByThreeDecimalPlaces (double nr1, double nr2) {

        if (nr1 == nr2) {
            return true;
        } else if (Math.abs(nr1-nr2) <= 0.00099) {
            return true;
        } else if (Math.abs(nr2-nr1) <= 0.00099) {
            return true;
        } else {
            return false;
        }
    }
}
