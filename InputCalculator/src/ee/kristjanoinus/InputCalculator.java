package ee.kristjanoinus;

import java.util.Scanner;

public class InputCalculator {

    public static void inputThenPrintSumAndAverage() {

        Scanner scanner = new Scanner(System.in);

        int count = 0;
        int sum = 0;
        long average = 0;

        while (true) {

            boolean isInt = scanner.hasNextInt();

            if (isInt) {
                int entry = scanner.nextInt();
                count++;
                sum += entry;
                average = Math.round((double) (sum) / count);
            } else {
                break;
            }


            scanner.nextLine();
        }

        System.out.println("SUM = " + sum + " AVG = " + average);
        scanner.close();
    }
}
