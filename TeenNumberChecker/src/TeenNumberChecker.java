public class TeenNumberChecker {

    public static boolean hasTeen (int nr1, int nr2, int nr3) {

        if ((nr1 > 12 && nr1 <20) || (nr2 > 12 && nr2 <20) || (nr3 > 12 && nr3 <20)) {
            return true;
        }
        return false;
    }

    public static boolean isTeen (int nr) {

        if (nr >= 13 && nr <= 19) {
            return true;
        }
        return false;
    }
}
