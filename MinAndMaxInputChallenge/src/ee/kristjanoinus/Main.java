package ee.kristjanoinus;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
//        boolean first = true;

        while (true) {

            System.out.println("Enter number:");

            boolean isInt = scanner.hasNextInt();

            if (isInt) {
                int entry = scanner.nextInt();

//                if (first) {
//                    first = false;
//                    min = entry;
//                    max = entry;
//                }

                if (entry < min) {
                    min = entry;
                }

                if (entry > max) {
                    max = entry;
                }

            } else {
                break;
            }

            scanner.nextLine(); // handle input
        }
        scanner.close();
        System.out.println("min = " + min + ", max = " + max);
    }
}
