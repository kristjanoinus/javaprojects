package ee.kristjanoinus;

import java.util.ArrayList;
import java.util.List;

public class GreatestCommonDivisor {

    public static int getGreatestCommonDivisor(int first, int second) {

        if ((first < 10) || (second < 10)) return -1;

        int greatestDivisior = 1;
        List<Integer> fDiv = new ArrayList<>();
        List<Integer> sDiv = new ArrayList<>();

        for (int i = 1; i <= first; i++) {
            if (first % i == 0) {
                fDiv.add(i);
            }
        }

        for (int j = 1; j <= second; j++) {
            if (second % j == 0) {
                sDiv.add(j);
            }
        }

        for (int k = 0; k < fDiv.size(); k++) {
            int fDivisor = fDiv.get(k);
            for (int l = 0; l < sDiv.size(); l++) {
                int sDivisor = sDiv.get(l);
                if (fDivisor == sDivisor) {

                    greatestDivisior = fDiv.get(k);

                }
            }

        }

        return greatestDivisior;
    }
}
