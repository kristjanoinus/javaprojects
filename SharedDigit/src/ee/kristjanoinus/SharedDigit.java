package ee.kristjanoinus;

public class SharedDigit {

    public static boolean hasSharedDigit (int first, int second) {

        if ((first < 10) || (first > 99) || (second < 10) || (second > 99)) return false;

        String firstString = Integer.toString(first);
        String secondString = Integer.toString(second);
        int firstLength = firstString.length();
        int secondLength = secondString.length();

        for (int i = 0; i < firstLength; i++) {
            String fs = "" + firstString.charAt(i);
            int fi = Integer.parseInt(fs);
            for (int j = 0; j < secondLength; j++) {
                String ss = "" + secondString.charAt(j);
                int si = Integer.parseInt(ss);
                if (fi == si) return true;
            }
        }
        return false;
    }
}
