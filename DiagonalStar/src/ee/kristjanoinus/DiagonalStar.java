package ee.kristjanoinus;

public class DiagonalStar {

    public static void printSquareStar (int number) {

        if (number < 5) {
            System.out.println("Invalid Value");
            return;
        }

        int rowCount = 1;
        for (int i = 1; i <= number; i++) {

            if (rowCount == (number + 1)) break;

            if ((i == 1)) {
                for (int j = 0; j < number; j++) {
                    System.out.print("*");
                }
                rowCount++;
                System.out.println();
                continue;
            }

            if ((i == (number))) {
                for (int j = 0; j < number; j++) {
                    System.out.print("*");
                }
                rowCount++;
                continue;

            }

            for (int k = 1; k <= number; k++) {

                if ((k == 1) || (k == (number))) {
                    System.out.print("*");
                } else if ((k == rowCount) || (k == (number - rowCount + 1))) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }

            }

            System.out.println();
            rowCount++;

        }
    }
}
