package ee.kristjanoinus;

public class Main {

    public static void main(String[] args) {
        System.out.println(isEvenNumber(5));


        int number = 4;
        int finishNumber = 20;
        int countEven = 0;

        while (number <= finishNumber) {
            number++;
            if (!isEvenNumber(number)) {
                continue;
            }

            countEven++;
            System.out.println("Even number" + number);
            if (countEven >= 5) {
                break;
            }
        }
        System.out.println("Total even numbers found = " + countEven);
    }

    public static boolean isEvenNumber (int number) {

        if ((number % 2) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
