package ee.kristjanoinus;

public class Room {

    private Windows windows;
    private Table table;

    public Room(Windows windows, Table table) {
        this.windows = windows;
        this.table = table;
    }

    public Windows getWindows() {
        return windows;
    }

    public void tableShape() {
        System.out.println("Room -> tableShaping");
        table.printTableShape();
    }
}
