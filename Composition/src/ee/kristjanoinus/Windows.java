package ee.kristjanoinus;

public class Windows {

    private boolean openable;
    private int count;

    public Windows(boolean openable, int count) {
        this.openable = openable;
        this.count = count;
    }

    public boolean isOpenable() {
        return openable;
    }

    public int getCount() {
        return count;
    }
}
