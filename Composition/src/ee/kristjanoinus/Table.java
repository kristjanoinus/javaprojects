package ee.kristjanoinus;

public class Table {

    private String shape;
    private int legs;

    public Table(String shape, int legs) {
        this.shape = shape;
        this.legs = legs;
    }

    public void printTableShape() {
        System.out.println(shape);
    }

    private String getShape() {
        return shape;
    }

    private int getLegs() {
        return legs;
    }
}
