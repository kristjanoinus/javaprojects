package ee.kristjanoinus;

public class Main {

    public static void main(String[] args) {

        int threeFiveCount = 0;
        int threeFiveSum = 0;
        for (int i = 1; i <= 1000; i++) {
            if ((i % 3 == 0) && (i % 5 == 0)) {
                System.out.println("Number that is evenly dividable by 3 and 5 is " + i);
                threeFiveCount++;
                threeFiveSum += i;
                if (threeFiveCount == 5) {
                    break;
                }
            }
        }
        System.out.println("The sum of found " + threeFiveCount + " numbers is " + threeFiveSum);
    }
}
