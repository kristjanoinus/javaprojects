package ee.kristjanoinus;

public class EvenDigitSum {

    public static int getEvenDigitSum (int number) {

        if (number < 0) return -1;

        String num = Integer.toString(number);
        int numLength = (int) (Integer.toString(number)).length();

        int sum = 0;
        for (int i = 0; i < numLength; i++) {
            String checkNumber = "" + (num.charAt(i));
            System.out.println(checkNumber);
            int checkNum = Integer.parseInt(checkNumber);
            if ((checkNum % 2) == 0) {
                sum += checkNum;
            }
        }
        return sum;
    }
}
