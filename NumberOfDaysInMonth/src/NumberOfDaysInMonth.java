package ee.kristjanoinus;

public class NumberOfDaysInMonth {

    public static boolean isLeapYear (int year) {

        boolean isLeap = false;

        if (year > 0 && year <= 9999) {
            if (((year % 4) == 0 && (year % 100) != 0) || (year % 400) == 0) {
                isLeap = true;
            }
        }
        return isLeap;
    }

    public static int getDaysInMonth (int month, int year) {

        int days = -1;
        if (month >= 1 && month <= 12 && year >= 0 && year <= 9999) {
            if (isLeapYear(year) && month == 2) {
                days = 29;
            } else if (!isLeapYear(year) && month == 2) {
                days = 28;
            } else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                days = 31;
            } else {
                days = 30;
            }
        }
        return days;
    }
}
