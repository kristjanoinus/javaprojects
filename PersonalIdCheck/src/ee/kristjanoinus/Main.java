package ee.kristjanoinus;

public class Main {

    public static void main(String[] args) {

        boolean checkResult = isPersonalIdCorrect(51107121760L);
        System.out.println(checkResult);

    }

    public static boolean isPersonalIdCorrect(long personalIdNr) {

        boolean valid = false;
        String personalId = Long.toString(personalIdNr);


        if (personalId.length() < 11 || personalId.length() > 11) {
            System.out.println("Check PersonalId, length is incorrect");
            valid = true;
            return valid;
        } else {
            int checkNumber = Integer.parseInt(personalId.substring(10, 11));
            int levelOneWeight = 0;
            for (int i = 0; i < 10; i++) {
                levelOneWeight += ((Integer.parseInt(personalId.substring(i, i + 1))) * (weightFactor(i)));
            }
            if (levelOneWeight % 11 != 10 && levelOneWeight % 11 == checkNumber) {
                System.out.println("Level ONE check correct. Personal ID is correct");
                valid = true;
            } else if (levelOneWeight % 11 == 10) {
                int levelTwoWeight = 0;
                for (int i = 0; i < 10; i++) {
                    levelTwoWeight += ((Integer.parseInt(personalId.substring(i, i + 1))) * (weightFactor(i + 2)));
                }
                if (levelTwoWeight % 11 != 10 && levelTwoWeight % 11 == checkNumber) {
                    System.out.println("Level TWO check correct. Personal ID is correct");
                    valid = true;
                } else if (levelTwoWeight % 11 == 10 && checkNumber == 0) {
                    System.out.println("Level TWO ZERO check correct. Personal ID is correct");
                    valid = true;
                }
            }
        }
            System.out.println("Error! Personal ID is incorrect");
            return valid;
        }

        private static int weightFactor ( int index){
            if (index + 1 > 9) {
                return index - 8;
            }
            return index + 1;
        }

    }
