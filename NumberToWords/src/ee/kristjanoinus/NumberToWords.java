package ee.kristjanoinus;

import java.util.ArrayList;
import java.util.List;

public class NumberToWords {

    public static void numberToWords(int number) {

        if (number < 0) System.out.println("Invalid Value");

        List<String> numbers = new ArrayList<>();
        int calcNumber = Math.abs(reverse(number));
        String numberString = Integer.toString(calcNumber);


        for (int i = 0; i < getDigitCount(number); i++) {

            int remainder = calcNumber % 10;
            String lastDigit = "";


            if (remainder == 0) {
                lastDigit = "Zero";
            } else if (remainder == 1) {
                lastDigit = "One";
            } else if (remainder == 2) {
                lastDigit = "Two";
            } else if (remainder == 3) {
                lastDigit = "Three";
            } else if (remainder == 4) {
                lastDigit = "Four";
            } else if (remainder == 5) {
                lastDigit = "Five";
            } else if (remainder == 6) {
                lastDigit = "Six";
            } else if (remainder == 7) {
                lastDigit = "Seven";
            } else if (remainder == 8) {
                lastDigit = "Eight";
            } else if (remainder == 9) {
                lastDigit = "Nine";
            }

            System.out.println(lastDigit);
            calcNumber /= 10;
        }


    }

    public static int reverse(int number) {
        int returnNumber = 0;
        int calcNumber = Math.abs(number);
        int tenFactor = getDigitCount(calcNumber) - 1;
        while (calcNumber != 0) {
            int remainder = calcNumber % 10;
            returnNumber = returnNumber + (remainder * ((int) Math.pow(10, tenFactor)));
            calcNumber /= 10;
            tenFactor--;
        }

        if (number < 0) returnNumber *= (-1);

        return returnNumber;
    }

    public static int getDigitCount(int number) {

        if (number < 0) return -1;

        int calcNumber = number;

        String numberString = Integer.toString(calcNumber);

        int digitCount = numberString.length();

        return digitCount;
    }
}
