package ee.kristjanoinus;

public class FlourPacker {

    public static boolean canPack (int bigCount, int smallCount, int goal) {

        boolean can = false;

        if ((bigCount < 0) || (smallCount) < 0 || (goal) < 0) {
            return false;
        }

        if (goal <= (bigCount * 5 + smallCount)
                && (smallCount >= ((goal - (bigCount * 5))))
                && (smallCount >= (goal % 5))) {
            can = true;
        }

        if (goal > (bigCount * 5 + smallCount)) can = false;

        return can;
    }

}
