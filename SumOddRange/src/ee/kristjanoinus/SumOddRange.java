package ee.kristjanoinus;

public class SumOddRange {

    public static boolean isOdd(int number) {

        boolean reply = false;
        if (number <= 0) reply = false;
        else if (number % 2 == 0) reply = false;
        else if (number % 2 != 0) reply = true;
        return reply;
    }

    public static int sumOdd(int start, int end) {

        int interimSum = 0;
        if ((start <= end) && (start > 0)) {
            for (int i = start; i <= end; i++) {
                if (isOdd(i)) interimSum += i;
            }
            return interimSum;
        } else {
            return -1;
        }
    }
}
