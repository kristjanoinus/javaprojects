package ee.kristjanoinus;

public class NumberPalindrome {

    public static boolean isPalindrome(int number) {

        if (number < 11 && number > -11) {
            return false;
        }
        int reverse = 0;
        int num = number;
        int remainder = 0;

        while (num != 0) {
            remainder = num % 10;
            reverse = reverse * 10 + remainder;
            num /= 10;

        }
            if (number == reverse && reverse != 0) {
                return true;
            }
            return false;
    }

}
