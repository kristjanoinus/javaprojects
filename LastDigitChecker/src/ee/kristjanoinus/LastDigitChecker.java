package ee.kristjanoinus;

public class LastDigitChecker {

    public static boolean hasSameLastDigit (int first, int second, int third) {

        if (!isValid(first) || !isValid(second) || !isValid(third)) {
            return false;
        }

        int fLast = first % 10;
        int sLast = second % 10;
        int tLast = third % 10;

        if ((fLast == sLast) || (fLast == tLast) || (sLast == tLast)) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isValid (int number) {

        if ((number > 9) && (number < 1001)) {
            return true;
        } else {
            return false;
        }
    }
}
