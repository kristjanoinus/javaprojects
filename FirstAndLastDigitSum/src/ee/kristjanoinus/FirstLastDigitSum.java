package ee.kristjanoinus;

public class FirstLastDigitSum {

    public static int sumFirstAndLastDigit (int number) {

        if (number < 0) return -1;

        int numLength = (int) (Integer.toString(number)).length();

        int last = number % 10;
        int first = number / (int) (Math.pow(10, (numLength - 1)));
        int sum = first + last;

        return sum;

    }
}
