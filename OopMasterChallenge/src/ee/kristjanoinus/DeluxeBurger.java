package ee.kristjanoinus;

public class DeluxeBurger extends Hamburger {

    public DeluxeBurger() {
        super("White bread", "Meat", 5, new DeluxeAddition("Chips"), new DeluxeAddition("Drink"), new Addition("N/A"), new Addition("N/A"));
    }

    @Override
    public String getPrice() {
        return super.getPrice();
    }
}

class DeluxeAddition extends Addition {

    private String name;
    private double price;

    public DeluxeAddition(String name) {
        super(name);
        if (name == "Chips") {
            super.setName("Chips");
            super.setPrice(1.9);
        } else if (name == "Drink") {
            super.setName("Drink");
            super.setPrice(2.0);
        } else if (name == "") {
            this.name = "N/A";
            this.price = 0;
        }
    }
}