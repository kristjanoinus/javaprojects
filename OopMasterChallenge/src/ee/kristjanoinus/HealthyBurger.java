package ee.kristjanoinus;

public class HealthyBurger extends Hamburger {

    private HealthyAddition addition5;
    private HealthyAddition addition6;

    public HealthyBurger(Addition addition1, Addition addition2, Addition addition3, Addition addition4, HealthyAddition addition5, HealthyAddition addition6) {
        super("Rye bread", "Chicken meat", 6, addition1, addition2, addition3, addition4);
        this.addition5 = addition5;
        this.addition6 = addition6;
    }

    @Override
    public String getPrice() {
        return getClass().getSimpleName() + "\n" +
                "Base price = " + getBasePrice() + "\n" +
                " extra " + getAddition1().getName() + " - " + getAddition1().getPrice() + " € \n" +
                " extra " + getAddition2().getName() + " - " + getAddition2().getPrice() + " € \n" +
                " extra " + getAddition3().getName() + " - " + getAddition3().getPrice() + " € \n" +
                " extra " + getAddition4().getName() + " - " + getAddition4().getPrice() + " € \n" +
                " extra " + getAddition5().getName() + " - " + getAddition5().getPrice() + " € \n" +
                " extra " + getAddition6().getName() + " - " + getAddition6().getPrice() + " € \n" +
                "TOTAL = " + (getBasePrice() + getAddition1().getPrice() + getAddition2().getPrice() +
                getAddition3().getPrice() + getAddition4().getPrice() +
                getAddition5().getPrice() + getAddition6().getPrice()) + " € \n";
    }

    public HealthyAddition getAddition5() {
        return addition5;
    }

    public HealthyAddition getAddition6() {
        return addition6;
    }
}

class HealthyAddition {

    private String name;
    private double price;

    public HealthyAddition(String name) {
        if (name == "Ruccola") {
            this.name = "Ruccola";
            this.price = 1.5;
        } else if (name == "Cabbage") {
            this.name = "Cabbage";
            this.price = 1.2;
        } else if (name == "") {
            this.name = "N/A";
            this.price = 0;
        }
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}