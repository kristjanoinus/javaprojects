package ee.kristjanoinus;

public class Hamburger {

    private String bread;
    private String meat;
    private double basePrice;
    private Addition addition1;
    private Addition addition2;
    private Addition addition3;
    private Addition addition4;

    public Hamburger(String bread, String meat, int basePrice, Addition addition1, Addition addition2, Addition addition3, Addition addition4) {
        this.bread = bread;
        this.meat = meat;
        this.basePrice = basePrice;
        this.addition1 = addition1;
        this.addition2 = addition2;
        this.addition3 = addition3;
        this.addition4 = addition4;
    }

    public String getPrice() {
        return getClass().getSimpleName() + "\n" +
                "Base price = " + basePrice + "\n" +
                " extra " + addition1.getName() + " - " + addition1.getPrice() + " € \n" +
                " extra " + addition2.getName() + " - " + addition2.getPrice() + " € \n" +
                " extra " + addition3.getName() + " - " + addition3.getPrice() + " € \n" +
                " extra " + addition4.getName() + " - " + addition4.getPrice() + " € \n" +
                "TOTAL = " + (basePrice + addition1.getPrice() + addition2.getPrice() +
                addition3.getPrice() + addition4.getPrice()) + " € \n";
    }

    public double getBasePrice() {
        return basePrice;
    }

    public Addition getAddition1() {
        return addition1;
    }

    public Addition getAddition2() {
        return addition2;
    }

    public Addition getAddition3() {
        return addition3;
    }

    public Addition getAddition4() {
        return addition4;
    }
}

class Addition {

    private String name;
    private double price;

    public Addition(String name) {
        if (name == "Lettuce") {
            this.name = "Lettuce";
            this.price = 1.1;
        } else if (name == "Tomato") {
            this.name = "Tomato";
            this.price = 1.2;
        } else if (name == "Carrot") {
            this.name = "Carrot";
            this.price = 1.3;
        } else if (name == "Onion") {
            this.name = "Onion";
            this.price = 0.9;
        } else if (name == "") {
            this.name = "N/A";
            this.price = 0;
        }
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}