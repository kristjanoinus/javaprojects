package ee.kristjanoinus;

public class LargestPrime {

    public static int getLargestPrime(int number) {

        int returnValue = -1;
        if (number < 0) return returnValue;

        for (int i = number / 2; i > 0; i--) {

            if ((number % i == 0)) {

                for (int j = 2; j <= i; j++) {

                    if ((i % j == 0) && (j < i)) {
                        break;
                    } else if ((i % j == 0) && (j == i)) {
                        returnValue = i;
                        return returnValue;
                    }
                }
                returnValue = i;

            }
        }
        if (returnValue == 1) {
            returnValue = number;
        }
        return returnValue;
    }

}